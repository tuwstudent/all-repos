SUBDIRS := $(filter-out out/., $(wildcard */.))

all: $(SUBDIRS)

$(SUBDIRS):
	@echo -n "$@ ... "
	@cd $@; git submodule init conf >> ../out/log.txt 2>&1; git submodule update conf >> ../out/log.txt 2>&1; make release >> ../out/log.txt 2>&1 && ([ $$? -eq 0 ] && echo "[OK]") || echo "[ERROR]"    
	@mkdir -p ./out/$@ || true
	@cp -r $@/conf/out/ ./out/$@/ || true

.PHONY: all $(SUBDIRS)
